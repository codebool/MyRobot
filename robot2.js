var Direction;
(function (Direction) {
    Direction[Direction["left"] = 0] = "left";
    Direction[Direction["right"] = 1] = "right";
})(Direction || (Direction = {}));
;
var AlertDisplay;
(function (AlertDisplay) {
    AlertDisplay["block"] = "block";
    AlertDisplay["none"] = "none";
})(AlertDisplay || (AlertDisplay = {}));
var breakTime = 2000;
var Robot = /** @class */ (function () {
    // 初始化的时候把窗口绑定进去
    function Robot(name, imgSrc) {
        this.name = name;
        this.movingDirection = Direction.right;
        this.threads = [{ id: null, interval: 100 }, { id: null, interval: 100 }];
        this.imgSrc = imgSrc;
        this.robot = undefined;
        this.alert = undefined;
        this.image = undefined;
        this.steps = undefined;
        this.windowWidth = undefined;
    }
    Robot.prototype.goRight = function (robotRight, stepSize, robotWidth, alert) {
        var _this = this;
        if (robotRight + stepSize >= this.windowWidth) {
            (this.robot).style.left = this.windowWidth - robotWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(function () { return _this.start(); }, breakTime);
        }
        else {
            (this.robot).style.left = (this.robot).offsetLeft + stepSize + 'px';
        }
    };
    Robot.prototype.goLeft = function (stepSize, alert) {
        var _this = this;
        if ((this.robot).offsetLeft - stepSize < 0) {
            (this.robot).left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(function () { return _this.start(); }, breakTime);
        }
        else {
            (this.robot).style.left = (this.robot).offsetLeft - stepSize + 'px';
        }
    };
    Robot.prototype.oneStep = function () {
        if (!this.steps) {
            this.steps = document.getElementById('stepSize');
        }
        if (!this.robot) {
            this.robot = document.getElementById('robot');
        }
        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }
        if (!this.windowWidth) {
            this.windowWidth = document.getElementById('window').clientWidth;
        }
        var stepSize = parseInt(this.steps.value);
        var robotWidth = parseInt(this.robot.style.width, 10);
        var robotRight = this.robot.offsetLeft + robotWidth;
        if (this.movingDirection === Direction.right) {
            this.goRight(robotRight, stepSize, robotWidth, this.alert);
        }
        else if (this.movingDirection === Direction.left) {
            this.goLeft(stepSize, this.alert);
        }
    };
    Robot.prototype.animation = function () {
        if (!this.image) {
            this.image = document.getElementById('image');
        }
        var pic = this.image.getAttribute("src");
        var pos = pic.indexOf('.');
        var num = undefined;
        if (-1 !== pos) {
            pos -= 1;
            num = pic[pos];
            num = num === '1' ? '2' : '1';
            (this.image).setAttribute('src', "image/D" + num + ".png");
        }
    };
    Robot.prototype.start = function () {
        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }
        (this.alert).style.display = AlertDisplay.none;
        this.threads[0].id = setInterval(this.oneStep.bind(this), this.threads[0].interval);
        this.threads[1].id = setInterval(this.animation.bind(this), this.threads[1].interval);
    };
    Robot.prototype.stop = function () {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    };
    return Robot;
}());
var r = new Robot("Robot_R", ["D1.jpg", "D2.jpg"]);
var startButton = document.getElementById('startButton');
var stopButton = document.getElementById('stopButton');
window.onload = function () {
    document.onkeydown = function (event) {
        var event = event || window.event;
        switch (event.keyCode) {
            case 39:
                r.oneStep();
                break;
        }
    };
};
function handleOneStep() {
    r.oneStep();
}
function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}
function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}
