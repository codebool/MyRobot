var AlertDisplay;
(function (AlertDisplay) {
    AlertDisplay["block"] = "block";
    AlertDisplay["none"] = "none";
})(AlertDisplay || (AlertDisplay = {}));
var Direction;
(function (Direction) {
    Direction[Direction["left"] = 0] = "left";
    Direction[Direction["right"] = 1] = "right";
    Direction[Direction["up"] = 2] = "up";
    Direction[Direction["down"] = 3] = "down";
})(Direction || (Direction = {}));
var keyCodes;
(function (keyCodes) {
    keyCodes[keyCodes["left"] = 37] = "left";
    keyCodes[keyCodes["up"] = 38] = "up";
    keyCodes[keyCodes["right"] = 39] = "right";
    keyCodes[keyCodes["down"] = 40] = "down";
})(keyCodes || (keyCodes = {}));
var breaktime = 2000;
var Robot = /** @class */ (function () {
    function Robot(name, imgSrc, robotSize) {
        this.name = name;
        this.threads = [{ id: null, interval: 100 }, { id: null, interval: 100 }];
        this.imgSrc = imgSrc;
        this.robot = document.getElementById('robot').setAttribute('width', robotSize + 'px');
        this.light = document.getElementById('light').setAttribute('width', robotSize + 'px');
        this.alert = undefined;
        this.steps = undefined;
        this.window = undefined;
        this.box = undefined;
        this.movingDirection = undefined;
        // window.onload(window.event);
        // window.onload = function() {
        //     document.onkeydown = function (event) {
        //         let event = event || window.event;
        //         switch (event.key) {
        //             case 39:
        //                 moveRight();
        //                 break;
        //         }
        //     }
        // }
    }
    Robot.prototype.getAllElements = function () {
        if (!this.steps) {
            this.steps = document.getElementById('steps');
        }
        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }
        if (!this.light) {
            this.light = document.getElementById('light');
        }
        if (!this.window) {
            this.window = document.getElementById('window').getBoundingClientRect();
        }
        if (!this.robot) {
            this.robot = document.getElementById('robot');
        }
        if (!this.box) {
            this.box = document.getElementById('box');
        }
    };
    Robot.prototype.moveUp = function () {
        this.getAllElements();
        var stepSize = parseInt(this.steps.value);
        var robotWidth = this.robot.width;
        var robotRight = this.robot.offsetLeft + robotWidth;
    };
    Robot.prototype.moveDown = function () {
        this.getAllElements();
        var stepSize = parseInt(this.steps.value);
        var robotWidth = this.robot.width;
        var robotRight = this.robot.offsetLeft + robotWidth;
    };
    Robot.prototype.moveLeft = function () {
        var _this = this;
        this.getAllElements();
        this.movingDirection = Direction.left;
        var stepSize = parseInt(this.steps.value);
        if ((this.box).offsetLeft - stepSize < 0) {
            (this.box).style.left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(function () { return _this.start(); }, breaktime);
        }
        else {
            (this.box).style.left = (this.box).offsetLeft - stepSize + 'px';
        }
        this.animation();
    };
    Robot.prototype.moveRight = function () {
        var _this = this;
        this.getAllElements();
        this.movingDirection = Direction.right;
        var stepSize = parseInt(this.steps.value);
        var boxWidth = parseInt(this.box.style.width, 10);
        var boxRight = parseInt(this.box.offsetLeft) + boxWidth;
        if (boxRight + stepSize >= this.window.width) {
            (this.box).left = this.window.width - boxWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(function () { return _this.start(); }, breaktime);
        }
        else {
            (this.box).style.left = (this.box).offsetLeft + stepSize + 'px';
        }
        this.animation();
    };
    Robot.prototype.oneStep = function () {
        if (this.movingDirection === Direction.up) {
            this.moveUp();
        }
        if (this.movingDirection === Direction.down) {
            this.moveDown();
        }
        if (this.movingDirection === Direction.left) {
            this.moveLeft();
        }
        if (this.movingDirection === Direction.right) {
            this.moveRight();
        }
    };
    Robot.prototype.animation = function () {
        this.getAllElements();
        var pic = this.robot.getAttribute("src");
        var pos = pic.indexOf('.');
        var num = undefined;
        if (-1 !== pos) {
            pos -= 1;
            num = pic[pos];
            num = num === '1' ? '2' : '1';
            (this.robot).setAttribute('src', "image/D" + num + ".png");
        }
    };
    Robot.prototype.start = function () {
        this.getAllElements();
        if (this.threads[0].id !== null) {
            clearInterval(this.threads[0].id);
            this.threads[0].id = null;
        }
        (this.alert).style.display = AlertDisplay.none;
        this.threads[0].id = setInterval(this.oneStep.bind(this), this.threads[0].interval);
    };
    Robot.prototype.stop = function () {
        clearInterval(this.threads[0].id);
    };
    return Robot;
}());
var r = new Robot("Robot_R", ["D1.jpg", "D2.jpg"], 100);
var startButton = document.getElementById('startButton');
var stopButton = document.getElementById('stopButton');
window.onload = function () {
    document.onkeydown = function (event) {
        var event = event || window.event;
        switch (event.keyCode) {
            case keyCodes.left:
                r.moveLeft();
                break;
            case keyCodes.up:
                r.moveUp();
                break;
            case keyCodes.right:
                r.moveRight();
                break;
            case keyCodes.down:
                r.moveDown();
                break;
        }
    };
};
function handleOneStep() {
    r.oneStep();
}
function moveRight() {
    r.moveRight();
}
function moveLeft() {
    r.moveLeft();
}
function moveUp() {
    r.moveUp();
}
function moveDown() {
    r.moveDown();
}
function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}
function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}
