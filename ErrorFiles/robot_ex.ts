enum Direction {
    left, right
}

enum Weapon {
    machineGun, pistol
}

interface AnimationTimer {
    id: number;
    interval: number;
}
//OOP: obejct oriented programming
// Base class -> Subclass

// encapsulation
// inheritance
//


// Overloading is not allowed in TypeScript (重载)

// Overloading occurs when two or more methods in one class have the same method name but different parameters.
// Overriding means having two methods with the same method name and parameters (i.e., method signature). （覆盖）
// One of the methods is in the parent class and the other is in the child class.
class Robot_ex {
    public color: string;
    public weight: number;
    public movingDirection: Direction;
    public stepSize: number;
    public window: string;
    public brand: string;
    private pin: string;
    public timers: AnimationTimer[];

    constructor(windowId: string) {
        console.log('executing...');
        this.color = 'white';
        this.weight = 30;
        this.movingDirection = Direction.right;
        this.stepSize = 10;
        this.brand = "WOW";
        this.window = windowId;
        this.pin = '8877';
        this.timers = [{id: null, interval: 1000}, {id: null, interval: 100}];
    }

    start() {
        this.timers[0].id = setInterval(this.step(), this.timers[0].interval);
    }

    step(): void {
        var mv = document.getElementById(this.window);
        console.log(this.window);
        console.log(typeof(this.window));
        console.log(mv);
        var step = this.stepSize;
        var rect = mv.getBoundingClientRect();
        console.log(rect);
        var browserRight = document.body.offsetLeft + window.innerWidth || document.body.clientWidth;
        mv.style.left = mv.offsetLeft + step + 'px';
    }

    oneStep(size: number):void {
        console.log('One step is: ' + size + 'px');
    }
}

//GRobot
// 1: payload
// 2: machineGun pistol
// 3: legs model

//HRobot
//1: carry
//2: greet
//3: climb


//pseudo => 伪代码
class GRobot extends Robot_ex {
    public payload: number;
    public weapon: Weapon;
    public legs: number;

    constructor(wp: Weapon, wnd: string) {
        super("wnd");
        this.weapon = wp;
        this.payload = undefined;
        this.legs = undefined;
    }
}

class HRobot extends Robot_ex {
    public carry: string;
    public greet: string;
    public climb: string;

    constructor(carry: string, greet: string, climb: string) {
        super("new wnd");
        this.carry = carry;
        this.greet = greet;
        this.climb = climb;
    }
}

let baseRobot = new Robot_ex('cube');
baseRobot.start();
let gRobot = new GRobot(Weapon.machineGun, 'SecondWnd');
let hRobot = new HRobot("tea", "morning", "stairs");

console.log(baseRobot);
console.log(gRobot);
console.log(hRobot);


function handleStart() {
    baseRobot.start();
}