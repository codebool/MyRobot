var Direction;
(function (Direction) {
    Direction[Direction["left"] = 0] = "left";
    Direction[Direction["right"] = 1] = "right";
})(Direction || (Direction = {}));
;
var AlertDisplay;
(function (AlertDisplay) {
    AlertDisplay["block"] = "block";
    AlertDisplay["none"] = "none";
})(AlertDisplay || (AlertDisplay = {}));
var ImgName;
(function (ImgName) {
    ImgName["one"] = "R1.jpg";
    ImgName["two"] = "R2.jpg";
})(ImgName || (ImgName = {}));
var Robot_ex2 = /** @class */ (function () {
    function Robot_ex2(name) {
        this.name = name;
        this.bodyImageSource = this.getBodyImageSource();
        this.cube = this.getBodyInfo();
        this.steps = this.getSteps();
        this.areaWidth = this.getAreaWidth();
        this.threads = [{ id: null, interval: 100 }, { id: null, interval: 100 }];
        this.movingDirection = Direction.right;
        this.imgName = ImgName.one;
        this.alertStyle = this.getAlertStatus();
    }
    Robot_ex2.prototype.getBodyImageSource = function () {
        return document.getElementById("image");
    };
    Robot_ex2.prototype.getBodyInfo = function () {
        return document.getElementById("cube");
    };
    Robot_ex2.prototype.getSteps = function () {
        return document.getElementById("stepSize");
    };
    Robot_ex2.prototype.getAreaWidth = function () {
        return document.getElementById("window").clientWidth;
    };
    Robot_ex2.prototype.getAlertStatus = function () {
        return document.getElementById("alert");
    };
    Robot_ex2.prototype.start = function () {
        var _this = this;
        this.alertStyle.style.display = AlertDisplay.none;
        this.threads[0].id = setInterval(function () { return _this.oneStep(); }, this.threads[0].interval);
        this.threads[1].id = setInterval(function () { return _this.bodyDancing(); }, this.threads[1].interval);
    };
    Robot_ex2.prototype.stop = function () {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    };
    Robot_ex2.prototype.bodyDancing = function () {
        console.log(this.bodyImageSource);
        var isImg = this.bodyImageSource;
        console.log(isImg);
        var str = isImg.src;
        console.log(str);
        var reverseStr = str.split('/').reverse();
        switch (reverseStr[0]) {
            case ImgName.one: {
                str = str.replace(ImgName.one, ImgName.two);
                this.bodyImageSource = str;
                break;
            }
            case ImgName.two: {
                str = str.replace(ImgName.two, ImgName.one);
                this.bodyImageSource = str;
                break;
            }
            default: {
                break;
            }
        }
    };
    Robot_ex2.prototype.oneStep = function () {
        var step = ((this.steps).value);
        var stepSizes = parseInt(step);
        var cube = this.cube;
        var cubeRight = cube.offsetLeft + parseInt(cube.style.width, 10);
        if (this.movingDirection = Direction.right) {
            this.goRight(cubeRight, stepSizes, this.alertStyle);
        }
        else {
            this.goLeft(stepSizes, this.alertStyle);
        }
    };
    Robot_ex2.prototype.goRight = function (cubeRight, stepSize, alertStyle) {
        if (cubeRight + stepSize * 2 >= this.areaWidth) {
            this.cube.style.left = this.cube.offsetLeft + 2 * (this.areaWidth - cubeRight) + 'px';
            alertStyle.style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(this.start, 2000);
        }
        else {
            this.cube.style.left = this.cube.offsetLeft + stepSize + 'px';
        }
    };
    Robot_ex2.prototype.goLeft = function (stepSize, alterStyle) {
        if (this.cube.offsetLeft - stepSize < 0) {
            this.cube.left = 0 + 'px';
            alterStyle.style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(this.start, 2000);
        }
        else {
            this.cube.style.left = this.cube.offsetLeft - stepSize + 'px';
        }
    };
    return Robot_ex2;
}());
var Robot001 = new Robot_ex2("Robot001");
function handleStart() {
    Robot001.start();
}
function handleStop() {
    Robot001.stop();
}
function handleOneStep() {
    Robot001.oneStep();
}
