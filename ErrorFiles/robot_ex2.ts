enum Direction {
    left, right
};

enum AlertDisplay {
    block = "block",
    none = "none"
}

enum ImgName {
    one = "R1.jpg",
    two = "R2.jpg"
}

interface Threads {
    id: number;
    interval: number;
}

class Robot_ex2 {
    public name: string;
    public bodyImageSource: any;
    public imgName: ImgName;
    public cube: any;
    public alertStyle: any;
    public steps: object;
    public areaWidth: number;
    public movingDirection: Direction;
    public threads: Threads[];

    constructor(name: string) {
        this.name = name;
        this.bodyImageSource = this.getBodyImageSource();
        this.cube = this.getBodyInfo();
        this.steps = this.getSteps();
        this.areaWidth = this.getAreaWidth();
        this.threads = [{id: null, interval: 100}, {id: null, interval: 100}];
        this.movingDirection = Direction.right;
        this.imgName = ImgName.one;
        this.alertStyle = this.getAlertStatus();
    }

    getBodyImageSource(): any {
        return document.getElementById(`image`);
    }

    getBodyInfo(): any {
        return document.getElementById(`cube`);
    }

    getSteps(): object {
        return document.getElementById(`stepSize`);
    }

    getAreaWidth(): number {
        return document.getElementById(`window`).clientWidth;
    }

    getAlertStatus(): any {
        return document.getElementById(`alert`);
    }

    start(): void {
        this.alertStyle.style.display = AlertDisplay.none;
        this.threads[0].id = setInterval(()=>this.oneStep(), this.threads[0].interval);
        this.threads[1].id = setInterval(()=>this.bodyDancing(), this.threads[1].interval);
    }

    stop(): void {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    }

    bodyDancing(): void {
        console.log(this.bodyImageSource);
        let isImg = (this.bodyImageSource as HTMLImageElement);
        console.log(isImg);
        let str = isImg.src;
        console.log(str);

        let reverseStr = str.split('/').reverse();
        switch (reverseStr[0]) {
            case ImgName.one: {
                str = str.replace(ImgName.one, ImgName.two);
                this.bodyImageSource = str;
                break;
            }

            case ImgName.two: {
                str = str.replace(ImgName.two, ImgName.one);
                this.bodyImageSource = str;
                break;
            }

            default: {
                break;
            }
        }
    }

    oneStep(): void {
        let step = ((this.steps).value);
        let stepSizes: number = parseInt(step);
        let cube = this.cube;
        let cubeRight = cube.offsetLeft + parseInt(cube.style.width, 10);
        if (this.movingDirection = Direction.right) {
            this.goRight(cubeRight, stepSizes, this.alertStyle);
        } else {
            this.goLeft(stepSizes, this.alertStyle);
        }
    }

    goRight(cubeRight: number, stepSize: number, alertStyle: any): void {
        if (cubeRight + stepSize*2 >= this.areaWidth) {
            this.cube.style.left = this.cube.offsetLeft + 2*(this.areaWidth-cubeRight) + 'px';
            alertStyle.style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(this.start, 2000);
        } else {
            this.cube.style.left = this.cube.offsetLeft + stepSize + 'px';
        }
    }

    goLeft(stepSize: number, alterStyle: any): void {
        if (this.cube.offsetLeft - stepSize < 0) {
            this.cube.left = 0 + 'px';
            alterStyle.style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(this.start, 2000);
        } else {
            this.cube.style.left = this.cube.offsetLeft - stepSize + 'px';
        }
    }

}

let Robot001 = new Robot_ex2(`Robot001`);

function handleStart(): void {
    Robot001.start();
}

function handleStop(): void {
    Robot001.stop();
}

function handleOneStep(): void {
    Robot001.oneStep();
}
