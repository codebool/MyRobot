var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Direction;
(function (Direction) {
    Direction[Direction["left"] = 0] = "left";
    Direction[Direction["right"] = 1] = "right";
})(Direction || (Direction = {}));
var Weapon;
(function (Weapon) {
    Weapon[Weapon["machineGun"] = 0] = "machineGun";
    Weapon[Weapon["pistol"] = 1] = "pistol";
})(Weapon || (Weapon = {}));
//OOP: obejct oriented programming
// Base class -> Subclass
// encapsulation
// inheritance
//
// Overloading is not allowed in TypeScript (重载)
// Overloading occurs when two or more methods in one class have the same method name but different parameters.
// Overriding means having two methods with the same method name and parameters (i.e., method signature). （覆盖）
// One of the methods is in the parent class and the other is in the child class.
var Robot_ex = /** @class */ (function () {
    function Robot_ex(windowId) {
        console.log('executing...');
        this.color = 'white';
        this.weight = 30;
        this.movingDirection = Direction.right;
        this.stepSize = 10;
        this.brand = "WOW";
        this.window = windowId;
        this.pin = '8877';
        this.timers = [{ id: null, interval: 1000 }, { id: null, interval: 100 }];
    }
    Robot_ex.prototype.start = function () {
        this.timers[0].id = setInterval(this.step(), this.timers[0].interval);
    };
    Robot_ex.prototype.step = function () {
        var mv = document.getElementById(this.window);
        console.log(this.window);
        console.log(typeof (this.window));
        console.log(mv);
        var step = this.stepSize;
        var rect = mv.getBoundingClientRect();
        console.log(rect);
        var browserRight = document.body.offsetLeft + window.innerWidth || document.body.clientWidth;
        mv.style.left = mv.offsetLeft + step + 'px';
    };
    Robot_ex.prototype.oneStep = function (size) {
        console.log('One step is: ' + size + 'px');
    };
    return Robot_ex;
}());
//GRobot
// 1: payload
// 2: machineGun pistol
// 3: legs model
//HRobot
//1: carry
//2: greet
//3: climb
//pseudo => 伪代码
var GRobot = /** @class */ (function (_super) {
    __extends(GRobot, _super);
    function GRobot(wp, wnd) {
        var _this = _super.call(this, "wnd") || this;
        _this.weapon = wp;
        _this.payload = undefined;
        _this.legs = undefined;
        return _this;
    }
    return GRobot;
}(Robot_ex));
var HRobot = /** @class */ (function (_super) {
    __extends(HRobot, _super);
    function HRobot(carry, greet, climb) {
        var _this = _super.call(this, "new wnd") || this;
        _this.carry = carry;
        _this.greet = greet;
        _this.climb = climb;
        return _this;
    }
    return HRobot;
}(Robot_ex));
var baseRobot = new Robot_ex('cube');
baseRobot.start();
var gRobot = new GRobot(Weapon.machineGun, 'SecondWnd');
var hRobot = new HRobot("tea", "morning", "stairs");
console.log(baseRobot);
console.log(gRobot);
console.log(hRobot);
function handleStart() {
    baseRobot.start();
}
