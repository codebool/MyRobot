enum Direction {
    left, right
};

enum AlertDisplay {
    block = "block",
    none = "none"
}

enum ImgName {
    one = "R1.jpg",
    two = "R2.jpg"
}

interface Threads {
    id: number;
    interval: number;
}

const breakTime: number = 2000;

class Robot {
    public name: string;
    public windowWidth: number;
    // public robotDOM: any;
    public robot: any;
    public alert: any;
    public image: any;
    public steps: any;
    public movingDirection: Direction;
    public threads: Threads[];

    // constructor(name: string, imgSrc: string[]) {}
    constructor(name: string) {
        this.name = name;
        this.windowWidth = document.getElementById(`window`).clientWidth;
        // this one is better, do not get every in constructor
        // this.robotDOM = undefined;
        this.robot = document.getElementById(`robot`);
        this.alert = document.getElementById(`alert`);
        this.image = document.getElementById(`image`) as HTMLImageElement;
        this.steps = document.getElementById(`stepSize`);
        this.movingDirection = Direction.right;
        this.threads = [{id: null, interval: 100}, {id: null, interval: 100}];
    }

    goRight(robotRight: number, stepSize: number, robotWidth: number, alert: any) {
        if (robotRight + stepSize >= this.windowWidth) {
            (this.robot).style.left = this.windowWidth - robotWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(()=>this.start(), 2000);
        } else {
            (this.robot).style.left = (this.robot).offsetLeft + stepSize + 'px';
        }
    }

    goLeft(stepSize: number, alert: any) {
        if ((this.robot).offsetLeft - stepSize < 0) {
            (this.robot).left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(()=>this.start(), breakTime);
        } else {
            (this.robot).style.left = (this.robot).offsetLeft - stepSize + 'px';
        }
    }

    oneStep() {
        let stepSize = parseInt(this.steps.value);
        let robotWidth = parseInt(this.robot.style.width, 10);
        let robotRight = this.robot.offsetLeft + robotWidth;
        if (this.movingDirection === Direction.right) {
            this.goRight(robotRight, stepSize, robotWidth, this.alert);
        } else if (this.movingDirection === Direction.left) {
            this.goLeft(stepSize, this.alert);
        }
    }

    bodyDancing() {
        let str = this.image.src;
        let reverseStr = str.split('/').reverse();
        switch (reverseStr[0]) {
            case ImgName.one: {
                str = str.replace(ImgName.one, ImgName.two);
                this.image.src = str;
                break;
            }

            case ImgName.two: {
                str = str.replace(ImgName.two, ImgName.one);
                this.image.src = str;
                break;
            }

            default: {
                break;
            }
        }
    }

    start() {
        (this.alert).style.display = AlertDisplay.none;
        // this.threads[0].id = setInterval(this.start.bind(this), 5000);
        // ---> this 跳出成为了windows, since setInterval是一个全局函数
        this.threads[0].id = setInterval(() => this.oneStep(), this.threads[0].interval);
        this.threads[1].id = setInterval(() => this.bodyDancing(), this.threads[1].interval);
    }

    stop() {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    }


    // 一定要重载， 因为每个动画都不一样
    // animation () {
    //     if (!this.robotDOM) {
    //         this.robotDOM = document.getElementById('robot');
    //     }
    //
    //     let robotImage = this.robotDOM.getAttribute('src');
    //     let pos = robotImage.indexOf('.'); // if exists, it will return the position index;
    //     let num = undefined;
    //     if (-1 !== pos) {
    //         pos -= 1;
    //         num = robotImage[pos];
    //         num = num === '1' ? '2' : '1';
    //         robotImage = `image/robot${num}.png`;
    //         robotImage.setAttribute('src', robotImage);
    //     }
    // }

}

let r = new Robot("Robot_R");
var startButton = <HTMLImageElement> document.getElementById('startButton');
var stopButton = <HTMLImageElement> document.getElementById('stopButton');

function handleOneStep() {
    r.oneStep();
}

function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}

function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}

// 根据keydown one step movement
// 撞墙后有反映
// 加一个alert light
// music