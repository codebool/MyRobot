interface Threads {
    id: number;
    interval: number;
}

enum AlertDisplay {
    block = "block",
    none = "none"
}

enum Direction {
    left, right, up, down
}

enum keyCodes {
    left = 37,
    up = 38,
    right = 39,
    down = 40
}

const breaktime: number = 2000;

class Robot {
    public name: string;
    public imgSrc: string[];
    public window: any;
    public alert: any;
    public light: any;
    public robot: any;
    public steps: any;
    public box: any;
    public movingDirection: Direction;
    public threads: Threads[];

    constructor(name: string, imgSrc: string[], robotSize: number) {
        this.name = name;
        this.threads = [{id: null, interval: 100}, {id: null, interval: 100}];
        this.imgSrc = imgSrc;
        this.robot = document.getElementById('robot').setAttribute('width', robotSize + 'px');
        this.light = document.getElementById('light').setAttribute('width', robotSize + 'px');
        this.alert = undefined;
        this.steps = undefined;
        this.window = undefined;
        this.box = undefined;
        this.movingDirection = undefined;
        // window.onload(window.event);
        // window.onload = function() {
        //     document.onkeydown = function (event) {
        //         let event = event || window.event;
        //         switch (event.key) {
        //             case 39:
        //                 moveRight();
        //                 break;
        //         }
        //     }
        // }

    }

    private getAllElements() {
        if (!this.steps) {
            this.steps = document.getElementById('steps');
        }
        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }
        if (!this.light) {
            this.light = document.getElementById('light');
        }
        if (!this.window) {
            this.window = document.getElementById('window').getBoundingClientRect();
        }
        if (!this.robot) {
            this.robot = document.getElementById('robot');
        }
        if (!this.box) {
            this.box = document.getElementById('box');
        }
    }

    moveUp() {
        this.getAllElements();
        let stepSize = parseInt(this.steps.value);
        let robotWidth = this.robot.width;
        let robotRight = this.robot.offsetLeft + robotWidth;
    }

    moveDown() {
        this.getAllElements();
        let stepSize = parseInt(this.steps.value);
        let robotWidth = this.robot.width;
        let robotRight = this.robot.offsetLeft + robotWidth;
    }

    moveLeft() {
        this.getAllElements();
        this.movingDirection = Direction.left;
        let stepSize = parseInt(this.steps.value);
        if ((this.box).offsetLeft - stepSize < 0) {
            (this.box).style.left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(()=>this.start(), breaktime);
        } else {
            (this.box).style.left = (this.box).offsetLeft - stepSize + 'px';
        }
        this.animation();
    }

    moveRight() {
        this.getAllElements();
        this.movingDirection = Direction.right;
        let stepSize = parseInt(this.steps.value);
        let boxWidth = parseInt(this.box.style.width, 10);
        let boxRight =parseInt(this.box.offsetLeft) + boxWidth;
        if (boxRight + stepSize >= this.window.width) {
            (this.box).left = this.window.width - boxWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(()=>this.start(), breaktime);
        } else {
            (this.box).style.left = (this.box).offsetLeft + stepSize + 'px';
        }
        this.animation();
    }

    oneStep(): void {
        if (this.movingDirection === Direction.up) {
            this.moveUp();
        }

        if (this.movingDirection === Direction.down) {
            this.moveDown();
        }

        if (this.movingDirection === Direction.left) {
            this.moveLeft();
        }

        if (this.movingDirection === Direction.right) {
           this.moveRight();
        }
    }

    animation(): void {
        this.getAllElements();
        let pic = this.robot.getAttribute(`src`);
        let pos = pic.indexOf('.');
        let num = undefined;
        if (-1 !== pos) {
            pos -= 1;
            num = pic[pos];
            num = num === '1' ? '2' : '1';
            (this.robot).setAttribute('src', `image/D${num}.png`);
        }
    }

    start(): void {
        this.getAllElements();
        if (this.threads[0].id !== null) {
            clearInterval(this.threads[0].id);
            this.threads[0].id = null;
        }
        (this.alert).style.display = AlertDisplay.none;
        this.threads[0].id = setInterval(this.oneStep.bind(this), this.threads[0].interval);
    }

    stop(): void {
        clearInterval(this.threads[0].id);
    }
}

let r = new Robot("Robot_R", ["D1.jpg", "D2.jpg"], 100);
var startButton = <HTMLImageElement> document.getElementById('startButton');
var stopButton = <HTMLImageElement> document.getElementById('stopButton');

window.onload = function() {
    document.onkeydown = function (event) {
        let event = event || window.event;
        switch (event.keyCode) {
            case keyCodes.left:
                r.moveLeft();
                break;

            case keyCodes.up:
                r.moveUp();
                break;

            case keyCodes.right:
                r.moveRight();
                break;

            case keyCodes.down:
                r.moveDown();
                break;
        }
    }
}

function handleOneStep() {
    r.oneStep();
}
function moveRight() {
    r.moveRight();
}

function moveLeft() {
    r.moveLeft();
}

function moveUp() {
    r.moveUp()
}

function moveDown() {
    r.moveDown()
}

function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}

function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}