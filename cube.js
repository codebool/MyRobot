var movingThread = undefined;
var bodyThread = undefined;
var runToRight = true;
var windowWidth = document.getElementById('window').clientWidth;
var steps = document.getElementById('stepSize');
var cube = document.getElementById('cube');
var gAlert = document.getElementById('alert');
var isImage = document.getElementById("image");
var imgName = new Array("R1.jpg", "R2.jpg");
function start() {
    var alert = gAlert;
    alert.style.display = "none";
    movingThread = setInterval(oneStep, 50);
    bodyThread = setInterval(bodyDancing, 50);
}
function stop() {
    clearInterval(movingThread);
    clearInterval(bodyThread);
}
function bodyDancing() {
    var str = isImage.src;
    var reverseStr = str.split('/').reverse();
    switch (reverseStr[0]) {
        case imgName[0]: {
            str = str.replace(imgName[0], imgName[1]);
            isImage.src = str;
            break;
        }
        case imgName[1]: {
            str = str.replace(imgName[1], imgName[0]);
            isImage.src = str;
            break;
        }
        default: {
            break;
        }
    }
}
function oneStep() {
    var stepSize = parseInt(steps.value);
    var cubeRight = cube.offsetLeft + parseInt(cube.style.width, 10);
    var alert = gAlert;
    if (runToRight) {
        goRight(cubeRight, stepSize, alert);
    }
    else {
        goLeft(stepSize, alert);
    }
}
function goRight(cubeRight, stepSize, alert) {
    if (cubeRight + stepSize * 2 >= windowWidth) {
        cube.style.left = cube.offsetLeft + 2 * (windowWidth - cubeRight) + 'px';
        alert.style.display = "block";
        stop();
        runToRight = false;
        setTimeout(start, 2000);
    }
    else {
        cube.style.left = cube.offsetLeft + stepSize + 'px';
    }
}
function goLeft(stepSize, alert) {
    if (cube.offsetLeft - stepSize < 0) {
        cube.style.left = '0 px';
        alert.style.display = "block";
        stop();
        runToRight = true;
        setTimeout(start, 2000);
    }
    else {
        cube.style.left = cube.offsetLeft - stepSize + 'px';
    }
}
// 机器人晃动， 加入MP3， 碰墙之后转头， 出现‘OOOPS’， sleep()两秒
// 两个及时器， the sepration of concerns 拒绝耦合
