enum Direction {
    left, right
};

enum AlertDisplay {
    block = "block",
    none = "none"
}

interface Threads {
    id: number;
    interval: number;
}

const breakTime: number = 2000;

class Robot {
    public name: string;
    public imgSrc: string[];
    public windowWidth: number;
    public robot: any;
    public alert: any;
    public image: any;
    public steps: any;
    public movingDirection: Direction;
    public threads: Threads[];

    // 初始化的时候把窗口绑定进去
    constructor(name: string, imgSrc: string[]) {
        this.name = name;
        this.movingDirection = Direction.right;
        this.threads = [{id: null, interval: 100}, {id: null, interval: 100}];
        this.imgSrc = imgSrc;
        this.robot = undefined;
        this.alert = undefined;
        this.image = undefined;
        this.steps = undefined;
        this.windowWidth = undefined;
    }

    goRight(robotRight: number, stepSize: number, robotWidth: number, alert: any): void {
        if (robotRight + stepSize >= this.windowWidth) {
            (this.robot).style.left = this.windowWidth - robotWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(()=>this.start(), breakTime);
        } else {
            (this.robot).style.left = (this.robot).offsetLeft + stepSize + 'px';
        }
    }

    goLeft(stepSize: number, alert: any): void {
        if ((this.robot).offsetLeft - stepSize < 0) {
            (this.robot).left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(()=>this.start(), breakTime);
        } else {
            (this.robot).style.left = (this.robot).offsetLeft - stepSize + 'px';
        }
    }

    oneStep(): void {
        if (!this.steps) {
            this.steps = document.getElementById('stepSize');
        }

        if (!this.robot) {
            this.robot = document.getElementById('robot');
        }

        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }

        if (!this.windowWidth) {
            this.windowWidth = document.getElementById('window').clientWidth;
        }

        let stepSize = parseInt(this.steps.value);
        let robotWidth = parseInt(this.robot.style.width, 10);
        let robotRight = this.robot.offsetLeft + robotWidth;
        if (this.movingDirection === Direction.right) {
            this.goRight(robotRight, stepSize, robotWidth, this.alert);
        } else if (this.movingDirection === Direction.left) {
            this.goLeft(stepSize, this.alert);
        }
    }

    animation(): void {
        if (!this.image) {
            this.image = document.getElementById('image');
        }
        let pic = this.image.getAttribute(`src`);
        let pos = pic.indexOf('.');
        let num = undefined;
        if (-1 !== pos) {
            pos -= 1;
            num = pic[pos];
            num = num === '1' ? '2' : '1';
            (this.image).setAttribute('src', `image/D${num}.png`);
        }
    }

    start(): void {
        if (!this.alert) {
            this.alert = document.getElementById('alert');
        }

        (this.alert).style.display = AlertDisplay.none;

        this.threads[0].id = setInterval(this.oneStep.bind(this), this.threads[0].interval);
        this.threads[1].id = setInterval(this.animation.bind(this), this.threads[1].interval);
    }

    stop(): void {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    }
}

let r = new Robot("Robot_R", ["D1.jpg", "D2.jpg"]);
var startButton = <HTMLImageElement> document.getElementById('startButton');
var stopButton = <HTMLImageElement> document.getElementById('stopButton');

window.onload = function() {
    document.onkeydown = function (event) {
        let event = event || window.event;
        switch (event.keyCode) {
            case 39:
                r.oneStep();
                break;
        }
    }
}

function handleOneStep() {
    r.oneStep();
}

function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}

function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}