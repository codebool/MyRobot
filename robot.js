var Direction;
(function (Direction) {
    Direction[Direction["left"] = 0] = "left";
    Direction[Direction["right"] = 1] = "right";
})(Direction || (Direction = {}));
;
var AlertDisplay;
(function (AlertDisplay) {
    AlertDisplay["block"] = "block";
    AlertDisplay["none"] = "none";
})(AlertDisplay || (AlertDisplay = {}));
var ImgName;
(function (ImgName) {
    ImgName["one"] = "R1.jpg";
    ImgName["two"] = "R2.jpg";
})(ImgName || (ImgName = {}));
var breakTime = 2000;
var Robot = /** @class */ (function () {
    // constructor(name: string, imgSrc: string[]) {}
    function Robot(name) {
        this.name = name;
        this.windowWidth = document.getElementById("window").clientWidth;
        // this one is better, do not get every in constructor
        // this.robotDOM = undefined;
        this.robot = document.getElementById("robot");
        this.alert = document.getElementById("alert");
        this.image = document.getElementById("image");
        this.steps = document.getElementById("stepSize");
        this.movingDirection = Direction.right;
        this.threads = [{ id: null, interval: 100 }, { id: null, interval: 100 }];
    }
    Robot.prototype.goRight = function (robotRight, stepSize, robotWidth, alert) {
        var _this = this;
        if (robotRight + stepSize >= this.windowWidth) {
            (this.robot).style.left = this.windowWidth - robotWidth + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.left;
            setTimeout(function () { return _this.start(); }, 2000);
        }
        else {
            (this.robot).style.left = (this.robot).offsetLeft + stepSize + 'px';
        }
    };
    Robot.prototype.goLeft = function (stepSize, alert) {
        var _this = this;
        if ((this.robot).offsetLeft - stepSize < 0) {
            (this.robot).left = 0 + 'px';
            (this.alert).style.display = AlertDisplay.block;
            this.stop();
            this.movingDirection = Direction.right;
            setTimeout(function () { return _this.start(); }, breakTime);
        }
        else {
            (this.robot).style.left = (this.robot).offsetLeft - stepSize + 'px';
        }
    };
    Robot.prototype.oneStep = function () {
        var stepSize = parseInt(this.steps.value);
        var robotWidth = parseInt(this.robot.style.width, 10);
        var robotRight = this.robot.offsetLeft + robotWidth;
        if (this.movingDirection === Direction.right) {
            this.goRight(robotRight, stepSize, robotWidth, this.alert);
        }
        else if (this.movingDirection === Direction.left) {
            this.goLeft(stepSize, this.alert);
        }
    };
    Robot.prototype.bodyDancing = function () {
        var str = this.image.src;
        var reverseStr = str.split('/').reverse();
        switch (reverseStr[0]) {
            case ImgName.one: {
                str = str.replace(ImgName.one, ImgName.two);
                this.image.src = str;
                break;
            }
            case ImgName.two: {
                str = str.replace(ImgName.two, ImgName.one);
                this.image.src = str;
                break;
            }
            default: {
                break;
            }
        }
    };
    Robot.prototype.start = function () {
        var _this = this;
        (this.alert).style.display = AlertDisplay.none;
        // this.threads[0].id = setInterval(this.start.bind(this), 5000);
        // ---> this 跳出成为了windows, since setInterval是一个全局函数
        this.threads[0].id = setInterval(function () { return _this.oneStep(); }, this.threads[0].interval);
        this.threads[1].id = setInterval(function () { return _this.bodyDancing(); }, this.threads[1].interval);
    };
    Robot.prototype.stop = function () {
        clearInterval(this.threads[0].id);
        clearInterval(this.threads[1].id);
    };
    return Robot;
}());
var r = new Robot("Robot_R");
var startButton = document.getElementById('startButton');
var stopButton = document.getElementById('stopButton');
function handleOneStep() {
    r.oneStep();
}
function handleStart() {
    r.start();
    startButton.disabled = true;
    stopButton.disabled = false;
}
function handleStop() {
    r.stop();
    stopButton.disabled = true;
    startButton.disabled = false;
}
// 根据keydown one step movement
// 撞墙后有反映
// 加一个alert light
// music
