var movingThread = undefined;
var bodyThread = undefined;
var runToRight = true;
var windowWidth = document.getElementById('window').clientWidth;
var steps = document.getElementById('stepSize');
var cube = document.getElementById('cube');
var gAlert = document.getElementById('alert');
var isImage = document.getElementById("image") as HTMLImageElement;
var imgName = new Array("R1.jpg", "R2.jpg");

function start(): void {
    let alert = gAlert;
    alert.style.display = "none";
    movingThread = setInterval(oneStep, 50);
    bodyThread = setInterval(bodyDancing, 50);
}

function stop(): void {
    clearInterval(movingThread);
    clearInterval(bodyThread);
}

function bodyDancing(): void {
    let str = isImage.src;
    let reverseStr = str.split('/').reverse();
    switch (reverseStr[0]) {
        case imgName[0]: {
            str = str.replace(imgName[0], imgName[1]);
            isImage.src = str;
            break;
        }

        case imgName[1]: {
            str = str.replace(imgName[1], imgName[0]);
            isImage.src = str;
            break;
        }

        default: {
            break;
        }
    }
}

function oneStep(): void {
    let stepSize = parseInt(steps.value);
    let cubeRight = cube.offsetLeft + parseInt(cube.style.width, 10);
    let alert = gAlert;
    if (runToRight) {
        goRight(cubeRight, stepSize, alert);
    } else {
        goLeft(stepSize, alert);
    }
}

function goRight(cubeRight: number, stepSize: number, alert: any) {
    if (cubeRight + stepSize*2 >= windowWidth) {
        cube.style.left = cube.offsetLeft + 2*(windowWidth - cubeRight) + 'px';
        alert.style.display = "block";
        stop();
        runToRight = false;
        setTimeout(start, 2000);
    } else {
        cube.style.left = cube.offsetLeft + stepSize + 'px';
    }
}

function goLeft(stepSize: number, alert: any) {
    if (cube.offsetLeft - stepSize < 0) {
        cube.style.left = '0 px';
        alert.style.display = "block";
        stop();
        runToRight = true;
        setTimeout(start, 2000);
    } else {
        cube.style.left = cube.offsetLeft - stepSize + 'px';
    }
}


// 机器人晃动， 加入MP3， 碰墙之后转头， 出现‘OOOPS’， sleep()两秒
// 两个及时器， the sepration of concerns 拒绝耦合